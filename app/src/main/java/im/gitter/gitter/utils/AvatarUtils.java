package im.gitter.gitter.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.DimenRes;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;

public class AvatarUtils {

    private final Resources resources;

    public AvatarUtils(Context context) {
        resources = context.getResources();
    }

    public String getAvatarWithDimen(String url, @DimenRes int id) {
        return url + "?s=" + resources.getDimensionPixelSize(id);
    }

    public Bitmap copyAsCircle(Bitmap bitmap) {
        // 1. create a circular drawable with the bitmap as the source
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(resources, bitmap);
        drawable.setCircular(true);

        // 2. create a new bitmap to draw onto
        Bitmap result = Bitmap.createBitmap(
                drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(),
                // alpha channel required for the cut away corners
                Bitmap.Config.ARGB_8888
        );

        // 3. draw the drawable onto the new bitmap
        Canvas canvas = new Canvas(result);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return result;
    }
}
