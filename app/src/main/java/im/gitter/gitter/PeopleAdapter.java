package im.gitter.gitter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;

import im.gitter.gitter.models.Room;
import im.gitter.gitter.network.VolleySingleton;
import im.gitter.gitter.utils.AvatarUtils;
import im.gitter.gitter.utils.CursorUtils;

public class PeopleAdapter extends RecyclerView.Adapter<RoomViewHolder> {

    private final AvatarUtils avatarUtils;
    private final ImageLoader imageLoader;
    private Cursor cursor;

    public PeopleAdapter(Context context) {
        avatarUtils = new AvatarUtils(context);
        imageLoader = VolleySingleton.getInstance(context).getImageLoader();
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        cursor.moveToPosition(position);
        Room room = Room.newInstance(CursorUtils.getContentValues(cursor));
        return room.getId().hashCode();
    }

    @Override
    public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View avatarWithBadgeView = LayoutInflater.from(parent.getContext()).inflate(R.layout.avatar_with_badge, parent, false);
        return new RoomViewHolder(avatarWithBadgeView);
    }

    @Override
    public void onBindViewHolder(RoomViewHolder holder, int position) {
        cursor.moveToPosition(position);
        Room room = Room.newInstance(CursorUtils.getContentValues(cursor));

        holder.avatarView.setImageUrl(avatarUtils.getAvatarWithDimen(room.getAvatarUrl(), R.dimen.avatar_with_badge_avatar_size), imageLoader);
        holder.avatarView.setDefaultImageResId(R.drawable.default_avatar);
        holder.titleView.setText(room.getName());
        holder.badgeView.setBadge(room.getUnreadCount(), room.getMentionCount(), room.hasActivity());
        holder.roomId = room.getId();
    }

    @Override
    public int getItemCount() {
        return cursor != null ? cursor.getCount() : 0;
    }

    public void setCursor(Cursor cursor) {
        if (this.cursor != cursor) {
            this.cursor = cursor;
            notifyDataSetChanged();
        }
    }
}
